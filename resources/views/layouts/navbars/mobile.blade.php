<div
    class="relative lg:hidden z-50"
    x-data="{ isOpen: false, timeStart: false }"
>

    <div class="fixed top-0 inset-0 w-full h-14 bg-white px-6 flex justify-between items-center shadow-md">

        <div class="flex">
            <button @click="isOpen= !isOpen , timeStart= false" class="text-black">
                <svg x-show="!isOpen" class="fill-current w-8" viewBox="0 0 32 32"><path d="M4,10h24c1.104,0,2-0.896,2-2s-0.896-2-2-2H4C2.896,6,2,6.896,2,8S2.896,10,4,10z M28,14H4c-1.104,0-2,0.896-2,2  s0.896,2,2,2h24c1.104,0,2-0.896,2-2S29.104,14,28,14z M28,22H4c-1.104,0-2,0.896-2,2s0.896,2,2,2h24c1.104,0,2-0.896,2-2  S29.104,22,28,22z"/></svg>
                <svg x-show="isOpen" class="fill-current w-8" x="0px" y="0px" viewBox="0 0 400.004 400.004"><g><path d="M382.688,182.686H59.116l77.209-77.214c6.764-6.76,6.764-17.726,0-24.485c-6.764-6.764-17.73-6.764-24.484,0L5.073,187.757c-6.764,6.76-6.764,17.727,0,24.485l106.768,106.775c3.381,3.383,7.812,5.072,12.242,5.072c4.43,0,8.861-1.689,12.242-5.072c6.764-6.76,6.764-17.726,0-24.484l-77.209-77.218h323.572c9.562,0,17.316-7.753,17.316-17.315C400.004,190.438,392.251,182.686,382.688,182.686z"/></g></svg>
            </button>
            <img class="ml-3" src="{{ asset('imgs/logo-default.png') }}" alt="">
        </div>

        <button class="text-black" x-on:click="timeStart = !timeStart, isOpen= false" >
            <svg class="fill-current w-10" viewBox="0 0 21 21"><g fill="currentColor" fill-rule="evenodd"><circle cx="10.5" cy="10.5" r="1"/><circle cx="10.5" cy="5.5" r="1"/><circle cx="10.5" cy="15.5" r="1"/></g></svg>
        </button>

    </div>

    <section
        :class="!isOpen ? 'animate-hidde-menu' : 'animate-show-menu' "
        class="fixed top-0 inset-0 z-[-100] w-[270px] pt-14 bg-white h-screen -translate-x-full"
    >
        <div class="w-full h-20 flex justify-center items-center space-x-4 border-b border-gray-200 text-gray-800">
            <a href="#">
                <svg class="fill-current w-5" viewBox="0 0 24 24"><path fill-rule="evenodd" d="M15.1742424,5.3203125 L17,5.3203125 L17,2.140625 C16.6856061,2.09765625 15.6022727,2 14.3409091,2 C11.7083333,2 9.90530303,3.65625 9.90530303,6.69921875 L9.90530303,9.5 L7,9.5 L7,13.0546875 L9.90530303,13.0546875 L9.90530303,22 L13.4659091,22 L13.4659091,13.0546875 L16.2537879,13.0546875 L16.6969697,9.5 L13.4659091,9.5 L13.4659091,7.05078125 C13.4659091,6.0234375 13.7424242,5.3203125 15.1742424,5.3203125 Z"/></svg>
            </a>
            <a href="#">
                <svg class="fill-current w-4" viewBox="0 0 512 512"><path d="M492,109.5c-17.4,7.7-36,12.9-55.6,15.3c20-12,35.4-31,42.6-53.6c-18.7,11.1-39.4,19.2-61.5,23.5 C399.8,75.8,374.6,64,346.8,64c-53.5,0-96.8,43.4-96.8,96.9c0,7.6,0.8,15,2.5,22.1C172,179,100.6,140.4,52.9,81.7 c-8.3,14.3-13.1,31-13.1,48.7c0,33.6,17.1,63.3,43.1,80.7C67,210.7,52,206.3,39,199c0,0.4,0,0.8,0,1.2c0,47,33.4,86.1,77.7,95 c-8.1,2.2-16.7,3.4-25.5,3.4c-6.2,0-12.3-0.6-18.2-1.8c12.3,38.5,48.1,66.5,90.5,67.3c-33.1,26-74.9,41.5-120.3,41.5 c-7.8,0-15.5-0.5-23.1-1.4C62.9,432,113.8,448,168.4,448C346.6,448,444,300.3,444,172.2c0-4.2-0.1-8.4-0.3-12.5 C462.6,146,479,128.9,492,109.5z"/></svg>
            </a>
            <a href="#">
                <svg class="fill-current w-6" viewBox="0 0 32 32" ><path d="M 11 7 C 6.027344 7 2 11.027344 2 16 C 2 20.972656 6.027344 25 11 25 C 15.972656 25 20 20.972656 20 16 C 20 15.382813 19.933594 14.78125 19.8125 14.199219 L 19.765625 14 L 11 14 L 11 17 L 17 17 C 16.523438 19.835938 13.972656 22 11 22 C 7.6875 22 5 19.3125 5 16 C 5 12.6875 7.6875 10 11 10 C 12.5 10 13.867188 10.554688 14.921875 11.464844 L 17.070313 9.359375 C 15.46875 7.894531 13.339844 7 11 7 Z M 25 11 L 25 14 L 22 14 L 22 16 L 25 16 L 25 19 L 27 19 L 27 16 L 30 16 L 30 14 L 27 14 L 27 11 Z"/></svg>
            </a>
            <a href="#">
                <svg class="fill-current w-4" viewBox="0 0 600 600"><g transform="matrix(1.01619,0,0,1.01619,44,43.8384)"><path d="M251.921,0.159C183.503,0.159 174.924,0.449 148.054,1.675C121.24,2.899 102.927,7.157 86.902,13.385C70.336,19.823 56.287,28.437 42.282,42.442C28.277,56.447 19.663,70.496 13.225,87.062C6.997,103.086 2.739,121.399 1.515,148.213C0.289,175.083 0,183.662 0,252.08C0,320.497 0.289,329.076 1.515,355.946C2.739,382.76 6.997,401.073 13.225,417.097C19.663,433.663 28.277,447.712 42.282,461.718C56.287,475.723 70.336,484.337 86.902,490.775C102.927,497.002 121.24,501.261 148.054,502.484C174.924,503.71 183.503,504 251.921,504C320.338,504 328.917,503.71 355.787,502.484C382.601,501.261 400.914,497.002 416.938,490.775C433.504,484.337 447.553,475.723 461.559,461.718C475.564,447.712 484.178,433.663 490.616,417.097C496.843,401.073 501.102,382.76 502.325,355.946C503.551,329.076 503.841,320.497 503.841,252.08C503.841,183.662 503.551,175.083 502.325,148.213C501.102,121.399 496.843,103.086 490.616,87.062C484.178,70.496 475.564,56.447 461.559,42.442C447.553,28.437 433.504,19.823 416.938,13.385C400.914,7.157 382.601,2.899 355.787,1.675C328.917,0.449 320.338,0.159 251.921,0.159ZM251.921,45.551C319.186,45.551 327.154,45.807 353.718,47.019C378.28,48.14 391.619,52.244 400.496,55.693C412.255,60.263 420.647,65.723 429.462,74.538C438.278,83.353 443.737,91.746 448.307,103.504C451.757,112.381 455.861,125.72 456.981,150.282C458.193,176.846 458.45,184.814 458.45,252.08C458.45,319.345 458.193,327.313 456.981,353.877C455.861,378.439 451.757,391.778 448.307,400.655C443.737,412.414 438.278,420.806 429.462,429.621C420.647,438.437 412.255,443.896 400.496,448.466C391.619,451.916 378.28,456.02 353.718,457.14C327.158,458.352 319.191,458.609 251.921,458.609C184.65,458.609 176.684,458.352 150.123,457.14C125.561,456.02 112.222,451.916 103.345,448.466C91.586,443.896 83.194,438.437 74.378,429.621C65.563,420.806 60.103,412.414 55.534,400.655C52.084,391.778 47.98,378.439 46.859,353.877C45.647,327.313 45.391,319.345 45.391,252.08C45.391,184.814 45.647,176.846 46.859,150.282C47.98,125.72 52.084,112.381 55.534,103.504C60.103,91.746 65.563,83.353 74.378,74.538C83.194,65.723 91.586,60.263 103.345,55.693C112.222,52.244 125.561,48.14 150.123,47.019C176.687,45.807 184.655,45.551 251.921,45.551Z"/><path d="M251.921,336.053C205.543,336.053 167.947,298.457 167.947,252.08C167.947,205.702 205.543,168.106 251.921,168.106C298.298,168.106 335.894,205.702 335.894,252.08C335.894,298.457 298.298,336.053 251.921,336.053ZM251.921,122.715C180.474,122.715 122.556,180.633 122.556,252.08C122.556,323.526 180.474,381.444 251.921,381.444C323.367,381.444 381.285,323.526 381.285,252.08C381.285,180.633 323.367,122.715 251.921,122.715Z"/><path d="M416.627,117.604C416.627,134.3 403.092,147.834 386.396,147.834C369.701,147.834 356.166,134.3 356.166,117.604C356.166,100.908 369.701,87.374 386.396,87.374C403.092,87.374 416.627,100.908 416.627,117.604Z"/></g></svg>
            </a>
        </div>

        <ul x-data="{ cars: false, pages: false }" class="mt-3 w-full h-full capitalize space-y-1">
            <li class="w-full bg-blue-500">
                <a href="{{ route('home') }}" class="w-full h-full py-3 pl-4 block text-sm text-white">home</a>
            </li>
            <li class="w-full bg-blue-5500">
                <a href="{{ route('about-us') }}" class="w-full h-full py-3 pl-4  block text-sm text-gray-400 text-wwhite">about us</a>
            </li>

            <li href="#" class="w-full py-3 pl-4 bg-blue-5500 flex justify-between text-sm text-gray-400 text-wwhite">
                <span>cars</span>
                <button
                    @click="cars = !cars"
                    class="w-4 h-4 mr-2"
                    :class="cars ? '-rotate-180' : '' "
                >
                    <svg class="fill-current w-4" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 13.172l4.95-4.95 1.414 1.414L12 16 5.636 9.636 7.05 8.222z"/></svg>
                </button>
            </li>
            <ul x-cloak x-show="cars" class="space-y-1">
                <li class="w-full bg-blue-5500">
                    <a href="#" class="w-full h-full block py-3 pl-4 text-sm text-gray-400 text-wwhite">our cars</a>
                </li>
                <li class="w-full bg-blue-5500">
                    <a href="#" class="w-full h-full block py-3 pl-4 text-sm text-gray-400 text-wwhite">classic car page</a>
                </li>
                <li class="w-full bg-blue-5500">
                    <a href="#" class="w-full h-full block py-3 pl-4 text-sm text-gray-400 text-wwhite">modern car page</a>
                </li>
                <li class="w-full bg-blue-5500">
                    <a href="#" class="w-full h-full block py-3 pl-4 text-sm text-gray-400 text-wwhite">grid car page</a>
                </li>
                <li class="w-full bg-blue-5500">
                    <a href="#" class="w-full h-full block py-3 pl-4 text-sm text-gray-400 text-wwhite">video car page</a>
                </li>
                <li class="w-full bg-blue-5500">
                    <a href="#" class="w-full h-full block py-3 pl-4 text-sm text-gray-400 text-wwhite">image car page</a>
                </li>
            </ul>

            <li class="w-full bg-blue-5500">
                <a href="{{ route('contact-us') }}" class="w-full h-full block py-3 pl-4 text-sm text-gray-400 text-wwhite">contact us</a>
            </li>

            <li class="w-full py-3 pl-4 text-sm bg-blue-5500 flex justify-between text-gray-400 text-wwhite">
                <span>page</span>
                <button
                    @click="pages = !pages"
                    class="w-4 h-4 mr-2"
                    :class="pages ? '-rotate-180' : '' "
                >
                    <svg class="fill-current w-4" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 13.172l4.95-4.95 1.414 1.414L12 16 5.636 9.636 7.05 8.222z"/></svg>
                </button>
            </li>
            <ul x-cloak x-show="pages" class="space-y-1">
                <h2 href="elements"></h2>
                <li class="w-full bg-blue-5500">
                    <a href="#" class="w-full h-full block py-3 pl-4 text-sm text-gray-400 text-wwhite">typografy</a>
                </li>
                <li class="w-full bg-blue-5500">
                    <a href="#" class="w-full h-full block py-3 pl-4 text-sm text-gray-400 text-wwhite">buttons</a>
                </li>
                <li class="w-full bg-blue-5500">
                    <a href="#" class="w-full h-full block py-3 pl-4 text-sm text-gray-400 text-wwhite">forms</a>
                </li>
                <li class="w-full bg-blue-5500">
                    <a href="#" class="w-full h-full block py-3 pl-4 text-sm text-gray-400 text-wwhite">tabs and accordios</a>
                </li>
                <li class="w-full bg-blue-5500">
                    <a href="#" class="w-full h-full block py-3 pl-4 text-sm text-gray-400 text-wwhite">counters</a>
                </li>
            </ul>

        </ul>
    </section>

    <section x-cloak
        :class="timeStart ? 'animate-show-timeStart' : 'animate-hidde-timeStart' "
        class="fixed top-14 right-0 z-[-100] w-60 p-5 bg-white translate-y-10 opacity-0"
    >
        <ul>
            <li class="flex justify-start items-center space-x-3">
                <div class="text-blue-500">
                    <svg class="fill-current w-[20px]" x="0px" y="0px" viewBox="0 0 16 16" style="enable-background:new 0 0 16 16;" xml:space="preserve"><g><g><path d="M8,0C3.589,0,0,3.589,0,8s3.589,8,8,8s8-3.589,8-8S12.411,0,8,0z M8,14c-3.309,0-6-2.691-6-6s2.691-6,6-6s6,2.691,6,6S11.309,14,8,14z"/><path d="M7,7H4v2h4c0.552,0,1-0.448,1-1V3H7V7z"/></g></g></svg>
                </div>
                <h1 class="capitalize text-base text-gray-700 font-normal">09:00AM — 17:00PM</h1>
            </li>
            <li class="mt-2 flex justify-start items-center space-x-3">
                <div class="text-blue-500">
                    <svg class="fill-current w-[20px]" viewBox="0 0 24 24"><path stroke-width="2" d="M6.375,2 C5,2 3,3.5 2.5,4.5 C1.78539513,5.92920973 1.9033374,6.49067969 2.375,8 C3,10 4.83244154,13.545116 7.375,16 C11,19.5 14.375,21 15.875,21.5 C17.375,22 19,21.5 20,20.5 C21,19.5 22,18.5 20.875,17 C20.077805,15.9370734 18.9164827,14.7082413 17.5,14 C16.2120164,13.3560082 15.444427,13.5904184 15,14.5 C14.7543142,15.0028302 14.6780041,15.9659877 14.5,16.5 C14.2754227,17.173732 13.375,17 12.375,16.5 C11.4176235,16.0213117 9,14 7,11 C5.76086515,9.14129772 7.74150655,9.12924672 9,8.5 C10,8 10.3099909,6.84998476 9.5,5.5 C8,3 7.5,2 6.375,2 Z"/></svg>
                </div>
                <div>
                    <h1 class="capitalize text-base text-gray-700 font-normal">+1-323-913-4688</h1>
                </div>
            </li>
        </ul>
        <button class="relative mt-4 w-full py-2.5 overflow-hidden uppercase text-center text-gray-900 text-sm font-light border border-black hover:border-blue-500 hover:bg-blue-500">get started</button>
    </section>

</div>

