<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8" />

        <title>{{ env('APP_NAME','Rental-Car') }}</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <link rel="icon" href="{{ asset('images/logo-default.png') }}">

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        @livewireStyles
    </head>
    <body class="font-poppins pt-14 lg:pt-0" style="background-image: url('{{ asset('gallery/hightway.jpeg') }}')">

        {{ $slot }}

        <script src="{{ asset('js/app.js') }}"></script>

        @livewireScripts
    </body>

</html>
