<div class="mx-auto mt-10 grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 items-center justify-center gap-5 lg:gap-8">
    <div class="flex justify-center items-center lg:py-5 border border-forth-gray">
        <img src="{{ asset('imgs/logo-mercedes.png') }}" alt="">
    </div>
    <div class="flex justify-center items-center lg:py-5 border border-forth-gray">
        <img src="{{ asset('imgs/logo-audi.png') }}" alt="">
    </div>
    <div class="flex justify-center items-center lg:py-5 border border-forth-gray">
        <img src="{{ asset('imgs/logo-tesla.png') }}" alt="">
    </div>
    <div class="flex justify-center items-center lg:py-5 border border-forth-gray">
        <img src="{{ asset('imgs/logo-citroen.png') }}" alt="">
    </div>
</div>
