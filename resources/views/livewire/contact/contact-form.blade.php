<div class="w-full lg:w-90 mx-auto py-20 px-4">
    <div class="md:flex justify-between items-center">
        <div class="md:w-[40%] flex-shrink-0 py-8 border-b md:border-b-0 md:border-r border-forth-gray">
            <h1 class="text-center text-3xl md:text-4xl text-second-gray font-light tracking-wider">Get in touch</h1>
        </div>

        <p class="my-5 p-6 text-xl text-center md:text-left tracking-wider leading-7">If you have any questions, just fill in the contact form, and we will answer you shortly.</p>
    </div>

    <form wire:submit.prevent="sendMessage()">
        
        <div class="mb-4 md:grid grid-cols-3 gap-3 space-y-4 md:space-y-0">
            <div class="relative">
                @error('state.name')
                    <span class="absolute top-1 right-2.5 text-[12px] text-red-500 font-light">{{ $message }}</span>
                @enderror

                <input wire:model="state.name" type="text" class="w-full py-5 px-6 bg-five-gray placeholder-second-gray text-sm" placeholder="Your Name">
            </div>

            <div class="relative">
                @error('state.email')
                    <span class="absolute top-1 right-2.5 text-[12px] text-red-500 font-light">{{ $message }}</span>
                @enderror

                <input wire:model="state.email" type="text" class="w-full py-5 px-6 bg-five-gray placeholder-second-gray text-sm" placeholder="E-mail">
            </div>

            <div class="relative">
                @error('state.phone')
                    <span class="absolute top-1 right-2.5 text-[12px] text-red-500 font-light">{{ $message }}</span>
                @enderror

                <input wire:model="state.phone" type="text" class="w-full py-5 px-6 bg-five-gray placeholder-second-gray text-sm" placeholder="Phone">
            </div>
        </div>

        <div class="relative">
            @error('state.body')
                <span class="absolute top-1 right-2.5 text-[12px] text-red-500 font-light">{{ $message }}</span>
            @enderror

            <textarea wire:model="state.body" class="w-full h-[300px] py-5 px-6 bg-five-gray placeholder-second-gray text-sm" placeholder="Message"></textarea>
        </div>

        @if (!$loaded)
        <button type="submit" class="mt-6 w-full md:w-auto md:px-10 py-4 uppercase text-center text-xl font-medium tracking-wide text-white bg-primary" wire:loading.class="!bg-gray-300">send message</button>
        @else
        <p class="w-full py-4 uppercase text-center text-xl font-medium tracking-wide text-white bg-primary">message sended</p>
        @endif

        <p wire:loading >Message Sending</p>
        <button class="py-4 capitalize text-sm font-medium tracking-wide text-first-gray">cancel</button>
    </form>
</div>
