
<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Information Car') }}
    </h2>
</x-slot>

<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
                <div class="w-[500px] h-[300px] mx-auto rounded-lg overflow-hidden">
                    <img src="{{ $this->car->image }}" class="object-cover" alt="{{ $this->car->name }}">
                </div>

                <div class="mt-10 w-[1000px] mx-auto">
                    <h1 class="mb-3 inline-block text-2xl font-medium uppercase tracking-wider text-primary shadow-lg">{{ $this->car->name }}</h1>

                    <ul class="divide-y divide-blue-500">
                        <li class="grid grid-cols-2 py-1.5">
                            <h1 class="capitalize">serial number :</h1>
                            <h1 class="uppercase text-lg font-medium text-primary">{{$this->car->pivot->serial}}</h1>
                        </li>

                        <li class="grid grid-cols-2 py-1.5">
                            <h1 class="capitalize">date start :</h1>
                            <h1 class="uppercase text-lg font-medium text-primary">{{$this->car->pivot->start}}</h1>
                        </li>

                        <li class="grid grid-cols-2 py-1.5">
                            <h1 class="capitalize">date end :</h1>
                            <h1 class="uppercase text-lg font-medium text-primary">{{$this->car->pivot->end}}</h1>
                        </li>

                        <li class="grid grid-cols-2 py-1.5">
                            <h1 class="capitalize">days :</h1>
                            <h1 class="uppercase text-lg font-medium text-primary">{{$this->car->daysRental}}<span class="lowercase text-first-gray"> /@if($this->car->daysRental < 2) day @else days @endif</span></h1>
                        </li>

                        <li class="grid grid-cols-2 py-1.5">
                            <h1 class="capitalize">price :</h1>
                            <h1 class="uppercase text-lg font-medium text-primary">{{$this->car->pivot->price}} $</h1>
                        </li>
                    </ul>

                    <button wire:click="downloadInvoice()">download Invoice</button>
                </div>
            </div>
        </div>
    </div>
</div>
