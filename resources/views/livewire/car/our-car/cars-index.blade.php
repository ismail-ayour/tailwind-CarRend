<div class="w-full flex flex-col space-y-11">
    @foreach ($this->allCars as $car)
        <x-cars.our-car.car-item :car="$car"/>
    @endforeach

    {{ $this->allCars->links() }}
</div>
