<div class="bg-white">
    <x-header image="bg-cars.jpg" >
        <div class="relative w-full py-16 lg:py-32 px-4 space-y-12">
            <h1 class="capitalize text-center text-5xl font-medium tracking-widest lg:tracking-[5px] leading-none text-white">our cars</h1>
        </div>
    </x-header>

    <div class="px-4 pb-16 lg:py-20 w-84 mx-auto space-x-6 flex lg:justify-between">
        <div class="py-16 lg:py-0 w-2/3">
            <livewire:car.our-car.cars-index />
        </div>

        <div class="space-y-10 w-[300px]">
            <livewire:car.our-car.search-form />

            <livewire:car.our-car.categories />

            <livewire:car.our-car.price-filter />

            <livewire:car.our-car.popular-cars />

            <livewire:car.our-car.reviews />

            <livewire:newsletter.our-car-newsletter />
        </div>
    </div>
</div>
