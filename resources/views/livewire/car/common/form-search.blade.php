<div action="#" class="flex">
    <div class="mx-auto w-auto lg:pt-4 space-y-2.5 lg:space-y-0 lg:space-x-4 text-gray-600 text-sm">

        <select wire:model="form.category" class="w-full lg:w-60 lg:h-12 flex-shrink py-3 pl-5 pr-3 capitalize">
            <option class="px-2 py-2" value="">any brand</option>

            @foreach ($this->categories as $category)
                <option value="{{ $category->id }}" class="px-2 py-2">{{ $category->name }}</option>
            @endforeach
        </select>

        <select wire:model="form.energy" class="w-full lg:w-60 lg:h-12 flex-shrink py-3 pl-5 pr-3 capitalize">
            <option class="px-2 py-2" value="">any energy</option>

            @foreach ($this->energies as $energy)
            <option value="{{ $energy->id }}" class="px-2 py-2">{{ $energy->name }}</option>
            @endforeach
        </select>

        <select wire:model="form.order" class="w-full lg:w-60 lg:h-12 flex-shrink py-3 pl-5 pr-3 capitalize">
            <option class="px-2 py-2">order price</option>
            <option value="asc" class="px-2 py-2">price low to high</option>
            <option value="desc" class="px-2 py-2">price high to low</option>
        </select>

        <button wire:click="search" class="w-full lg:w-32 lg:h-12 flex-shrink bg-blue-500 py-3 lg:py-0 uppercase text-white lg:text-xs lg:font-medium">search</button>
    </div>
</div>
