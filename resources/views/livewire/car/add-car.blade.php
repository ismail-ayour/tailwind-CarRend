<div class="relative w-90 mx-auto border rounded-md py-5 px-10 space-y-4">
    @if (session()->has('message'))
        <div class="absolute right-5 top-3 px-20 py-4 bg-green-400 border border-green-600 drop-shadow-lg">
            <h1 class="text-xl font-medium tracking-wide text-green-700">{{ session('message') }}</h1>
        </div>
    @endif

    <h1 class="text-2xl font-medium inline-block text-primary tracking-wider border-b border-primary">create new car</h1>

    <div class="relative space-y-1 flex flex-col">
        <label class="capitalize font-medium text-blue-500" for="name">name:</label>

        <input class="px-2 py-1 text-primary text-lg font-medium tracking-wider w-64 border border-second-gray focus:border-0 focus:outline outline-offset-0 outline-1 outline-primary" id="name" type="text" wire:model.lazy="state.name" />

        @error('state.name') <span class="text-sm font-light text-red-500 tracking-wide">{{ $message }}</span> @enderror
    </div>

    <div class="grid grid-cols-2 gap-3 space-y-3">
        <div class="space-y-1 flex flex-col">
            <label class="capitalize font-medium text-blue-500" for="price">price:</label>

            <input class="px-2 py-1 text-primary text-lg font-medium tracking-wider w-64 border border-second-gray focus:border-0 focus:outline outline-offset-0 outline-1 outline-primary" id="price" type="text" wire:model.lazy="state.price" />

            @error('state.price') <span class="text-sm font-light text-red-500 tracking-wide">{{ $message }}</span> @enderror
        </div>

        <div class="space-y-1 flex flex-col">
            <label class="capitalize font-medium text-blue-500" for="number">number:</label>

            <input class="px-2 py-1 text-primary text-lg font-medium tracking-wider w-64 border border-second-gray focus:border-0 focus:outline outline-offset-0 outline-1 outline-primary" id="number" type="number" wire:model.lazy="state.number" />

            @error('state.number') <span class="text-sm font-light text-red-500 tracking-wide">{{ $message }}</span> @enderror
        </div>

        <div class="space-y-1 flex flex-col">
            <label class="capitalize font-medium text-blue-500" for="wallet">wallet:</label>

            <input class="px-2 py-1 text-primary text-lg font-medium tracking-wider w-64 border border-second-gray focus:border-0 focus:outline outline-offset-0 outline-1 outline-primary" id="wallet" type="number" wire:model.lazy="state.wallet" />

            @error('state.wallet') <span class="text-sm font-light text-red-500 tracking-wide">{{ $message }}</span> @enderror
        </div>

        <div class="space-y-1 flex flex-col">
            <label class="capitalize font-medium text-blue-500" for="chair">chair:</label>

            <input class="px-2 py-1 text-primary text-lg font-medium tracking-wider w-64 border border-second-gray focus:border-0 focus:outline outline-offset-0 outline-1 outline-primary" id="chair" type="number" wire:model.lazy="state.chair" />

            @error('state.chair') <span class="text-sm font-light text-red-500 tracking-wide">{{ $message }}</span> @enderror
        </div>

        <div class="space-y-1 flex flex-col">
            <label class="capitalize font-medium text-blue-500" for="door">door:</label>

            <input class="px-2 py-1 text-primary text-lg font-medium tracking-wider w-64 border border-second-gray focus:border-0 focus:outline outline-offset-0 outline-1 outline-primary" id="door" type="number" wire:model.lazy="state.door" />

            @error('state.door') <span class="text-sm font-light text-red-500 tracking-wide">{{ $message }}</span> @enderror
        </div>
    </div>

    <div class="grid grid-cols-2 gap-x-3">
        <div class="space-y-1 w-64">
            <label class="capitalize font-medium text-blue-500" for="category">category:</label>

            <select class="px-2 py-1" wire:model="category">
                <option disabled>--selec category</option>

                @foreach ($this->categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>

            @error('category') <span class="text-sm font-light text-red-500 tracking-wide">{{ $message }}</span> @enderror
        </div>

        <div class="space-y-1 w-64">
            <label class="capitalize font-medium text-blue-500" for="energy">energy:</label>

            <select class="px-2 py-1" wire:model="energy">
                <option disabled>--selec energy</option>

                @foreach ($this->energies as $energy)
                    <option value="{{ $energy->id }}">{{ $energy->name }}</option>
                @endforeach
            </select>

            @error('energy') <span class="text-sm font-light text-red-500 tracking-wide">{{ $message }}</span> @enderror
        </div>
    </div>

    <div class="space-y-1 w-64">
        <input type="file" multiple wire:model="images">

        @error('images') <span class="text-sm font-light text-red-500 tracking-wide">{{ $message }}</span> @enderror
    </div>

    <button wire:click="store" class="px-5 py-2 bg-primary text-white">Save</button>
</div>
