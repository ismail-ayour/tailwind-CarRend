<div class="relative w-full lg:py-16">
    <img src="{{ $this->header->image->urlImage }}" class="absolute w-full h-full object-cover inset-0">
    <div class="absolute inset-0  bg-gray-600/40"></div>

    <div class="relative w-full py-16 lg:py-32 px-4 space-y-12">
        {{-- <h1 class="capitalize text-center text-4xl lg:text-7xl font-semibold tracking-[5px] leading-none text-white">find <span class="lowercase">the</span> best car</h1> --}}
        <h1 class="capitalize text-center text-4xl lg:text-7xl font-semibold tracking-[5px] leading-none text-white">{{ $this->header->title }}</h1>

        <p class="text-2xl lg:text-xl text-center text-gray-300 leading-8 tracking-wider">{{ $this->header->teaser }}</p>
        {{-- <p class="text-2xl lg:text-xl text-center text-gray-300 leading-8 tracking-wider">From as low as $10 per day with limited time offer discounts</p> --}}

        <livewire:car.common.form-search />
    </div>
</div>
