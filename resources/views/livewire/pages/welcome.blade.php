<div>
    <livewire:car.header />

    <livewire:cars.first-class />

    <livewire:why-choose-us.why-choose-us-welcome />

    <livewire:cars.popular-cars />

    <livewire:section.find-your-vehicle-by-type />

    <livewire:section.experiences />

    <livewire:section.happy-clients />

    <livewire:section.how-we-work />

    <livewire:section.questions.popular-questions />
</div>
