<div>
    <x-header image="bg-about.jpg" >
        <div class="relative w-full py-16 lg:py-32 px-4 space-y-12">
            <h1 class="capitalize text-center text-4xl lg:text-6xl font-semibold tracking-[5px] leading-none text-white">about us</h1>
        </div>
    </x-header>

    <livewire:why-choose-us.why-choose-us-about-us />

    <livewire:pages.about-us.our-history />

    <livewire:pages.about-us.happy-clients />

    <section class="object-cover py-8 lg:py-20 px-12" style="background-image: url( {{ asset('imgs/bg-counter-22.jpg') }} ); background-position: center center; background-size: cover">
        <div class="xl:w-[1190px] mx-auto grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 divide-y md:divide-none divide-forth-gray">
            <div class="relative pt-3 pb-6 text-center text-white md:before:absolute md:before:right-0 md:before:top-1/2 md:before:-translate-y-1/2 md:before:h-1/2 md:before:w-[2px] md:before:bg-forth-gray">
                <h1 class="text-5xl font-medium tracking-wide">12</h1>
                <h3 class="mt-7 text-xl capitalize tracking-wide">partners</h3>
            </div>

            <div class="relative pt-3 pb-6 text-center text-white lg:before:absolute lg:before:right-0 lg:before:top-1/2 lg:before:-translate-y-1/2 lg:before:h-1/2 lg:before:w-[2px] lg:before:bg-forth-gray">
                <h1 class="text-5xl font-medium tracking-wide">194</h1>
                <h3 class="mt-7 text-xl capitalize tracking-wide">cars</h3>
            </div>

            <div class="relative pt-3 pb-6 text-center text-white md:before:absolute md:before:right-0 md:before:top-1/2 md:before:-translate-y-1/2 md:before:h-1/2 md:before:w-[2px] md:before:bg-forth-gray">
                <h1 class="text-5xl font-medium tracking-wide">5K</h1>
                <h3 class="mt-7 text-xl capitalize tracking-wide">customers</h3>
            </div>
            <div class="pt-3 pb-6 text-center text-white">
                <h1 class="text-5xl font-medium tracking-wide">25</h1>
                <h3 class="mt-7 text-xl capitalize tracking-wide">team members</h3>
            </div>
        </div>
    </section>

    <section class="py-14 lg:py-24 xl:w-[1190px] mx-auto">
    <div class="px-4">
            <h1 class="mb-4 capitalize text-center text-3xl md:text-4xl font-medium text-gray-800">popular brands</h1>

            <livewire:common.type />
        </div>
    </section>

</div>
