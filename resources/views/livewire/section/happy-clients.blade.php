<div class="w-full py-16 md:py-24 bg-five-gray">
    <div class="w-full lg:w-84 mx-auto px-4 md:px-8 lg:px-0">
        <h1 class="capitalize text-center text-3xl md:text-4xl font-medium text-gray-800">happy clients</h1>

        <x-comments.index-happy-clients />
    </div>
</div>
