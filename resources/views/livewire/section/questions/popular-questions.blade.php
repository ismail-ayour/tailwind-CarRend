<div class="w-full py-14 md:py-24 bg-five-gray">
    <div class="w-full lg:w-84 mx-auto px-4 lg:px-0">
        <h1 class="mb-14 capitalize text-center text-3xl md:text-4xl font-medium text-gray-800">popular questions</h1>

        <div class="flex flex-col lg:flex-row-reverse justify-center items-start gap-8">
            <div class="h-full w-full md:w-[580px] lg:w-full mx-auto">
                <iframe class="w-full object-cover h-72 lg:h-96" src="https://www.youtube.com/embed/0IQ89vtZ8po" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
            </div>

            <x-sections.popular-questions />
        </div>
    </div>
</div>
