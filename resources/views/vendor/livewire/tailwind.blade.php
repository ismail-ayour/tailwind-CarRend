<div>
    @if ($paginator->hasPages())
        @php(isset($this->numberOfPaginatorsRendered[$paginator->getPageName()]) ? $this->numberOfPaginatorsRendered[$paginator->getPageName()]++ : $this->numberOfPaginatorsRendered[$paginator->getPageName()] = 1)

        <nav role="navigation" aria-label="Pagination Navigation" class="flesx items-center">
            <div class="text-blue-500 flex justify-between lg:justify-center space-x-2 lg:space-x-4 px-10">
                    <span>
                        {{-- Previous Page Link --}}
                        @if ($paginator->onFirstPage())
                            <span aria-disabled="true" aria-label="{{ __('pagination.previous') }}" class="flex items-center justify-center w-10 h-10 lg:w-12 lg:h-12 bg-gray-50 border border-blue-400 rounded-full shadow-xl">
                                <svg class="fill-current w-3" x="0px" y="0px" viewBox="0 0 96.154 96.154" style="enable-background:new 0 0 96.154 96.154;"xml:space="preserve"><g><path d="M75.183,0.561L17.578,46.513c-0.951,0.76-0.951,2.367,0,3.126l57.608,45.955c0.689,0.547,1.717,0.709,2.61,0.414 c0.186-0.061,0.33-0.129,0.436-0.186c0.65-0.351,1.057-1.025,1.057-1.765V2.093c0-0.736-0.405-1.414-1.057-1.762 c-0.108-0.059-0.253-0.127-0.426-0.184C76.903-0.15,75.874,0.011,75.183,0.561z"/></g></svg>
                            </span>
                        @else
                            {{-- <button wire:click="previousPage('{{ $paginator->getPageName() }}')" dusk="previousPage{{ $paginator->getPageName() == 'page' ? '' : '.' . $paginator->getPageName() }}.after" rel="prev" class="relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 rounded-l-md leading-5 hover:text-gray-400 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-500 transition ease-in-out duration-150" aria-label="{{ __('pagination.previous') }}">
                                <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                                    <path fill-rule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg>
                            </button> --}}

                            <button wire:click="previousPage('{{ $paginator->getPageName() }}')" dusk="previousPage{{ $paginator->getPageName() == 'page' ? '' : '.' . $paginator->getPageName() }}.after" aria-label="{{ __('pagination.previous') }}" class="flex items-center justify-center w-10 h-10 lg:w-12 lg:h-12 bg-gray-50 border border-blue-400 rounded-full shadow-xl">
                                <svg class="fill-current w-3" x="0px" y="0px" viewBox="0 0 96.154 96.154" style="enable-background:new 0 0 96.154 96.154;"xml:space="preserve"><g><path d="M75.183,0.561L17.578,46.513c-0.951,0.76-0.951,2.367,0,3.126l57.608,45.955c0.689,0.547,1.717,0.709,2.61,0.414 c0.186-0.061,0.33-0.129,0.436-0.186c0.65-0.351,1.057-1.025,1.057-1.765V2.093c0-0.736-0.405-1.414-1.057-1.762 c-0.108-0.059-0.253-0.127-0.426-0.184C76.903-0.15,75.874,0.011,75.183,0.561z"/></g></svg>
                            </button>
                        @endif
                    </span>

                    {{-- Pagination Elements --}}
                    @foreach ($elements as $element)

                        {{-- Array Of Links --}}
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                <span wire:key="paginator-{{ $paginator->getPageName() }}-{{ $this->numberOfPaginatorsRendered[$paginator->getPageName()] }}-page{{ $page }}">
                                    @if ($page == $paginator->currentPage())
                                        <span aria-current="page" class="flex items-center justify-center w-10 h-10 lg:w-12 lg:h-12 bg-gray-50 border border-blue-400 rounded-full shadow-xl">
                                            <h4 class="text-lg lg:text-2xl">{{ $page }}</h4>
                                        </span>
                                    @else
                                        <button wire:click="gotoPage({{ $page }}, '{{ $paginator->getPageName() }}')" aria-label="{{ __('Go to page :page', ['page' => $page]) }}" class="flex items-center justify-center w-10 h-10 lg:w-12 lg:h-12 bg-gray-50 border border-gray rounded-full shadow-xl">
                                            <h4 class="text-lg lg:text-2xl text-first-gray">{{ $page }}</h4>
                                        </button>
                                    @endif
                                </span>
                            @endforeach
                        @endif
                    @endforeach

                    {{-- Next Page Link --}}
                    @if ($paginator->hasMorePages())
                        <button wire:click="nextPage('{{ $paginator->getPageName() }}')" dusk="nextPage{{ $paginator->getPageName() == 'page' ? '' : '.' . $paginator->getPageName() }}.after" aria-label="{{ __('pagination.next') }}" class="flex items-center justify-center w-10 h-10 lg:w-12 lg:h-12 bg-gray-50 border border-gray rounded-full shadow-xl">
                            <svg class="fill-current w-3" x="0px" y="0px" viewBox="0 0 96.155 96.155" style="enable-background:new 0 0 96.155 96.155;"><g><path d="M20.972,95.594l57.605-45.951c0.951-0.76,0.951-2.367,0-3.127L20.968,0.56c-0.689-0.547-1.716-0.709-2.61-0.414 c-0.186,0.061-0.33,0.129-0.436,0.186c-0.65,0.35-1.056,1.025-1.056,1.764v91.967c0,0.736,0.405,1.414,1.056,1.762 c0.109,0.06,0.253,0.127,0.426,0.185C19.251,96.305,20.281,96.144,20.972,95.594z"/></g></svg>
                        </button>
                    @else
                        <span aria-disabled="true" aria-label="{{ __('pagination.next') }}" class="flex items-center justify-center w-10 h-10 lg:w-12 lg:h-12 bg-gray-50 border border-gray rounded-full shadow-xl">
                            <svg class="fill-current w-3" x="0px" y="0px" viewBox="0 0 96.155 96.155" style="enable-background:new 0 0 96.155 96.155;"><g><path d="M20.972,95.594l57.605-45.951c0.951-0.76,0.951-2.367,0-3.127L20.968,0.56c-0.689-0.547-1.716-0.709-2.61-0.414 c-0.186,0.061-0.33,0.129-0.436,0.186c-0.65,0.35-1.056,1.025-1.056,1.764v91.967c0,0.736,0.405,1.414,1.056,1.762 c0.109,0.06,0.253,0.127,0.426,0.185C19.251,96.305,20.281,96.144,20.972,95.594z"/></g></svg>
                        </span>
                    @endif
            </div>
        </nav>
    @endif
</div>
