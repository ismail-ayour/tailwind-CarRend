<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8" />
        <title>{{ env('APP_NAME','Rental-Car') }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        @livewireStyles
    </head>
    <body class="antialiased bg-white font-poppins pt-14 lg:pt-0">
        @include('layouts.navbars.mobile')
        @include('layouts.navbars.web')

        {{ $slot }}

        <footer>
            <section class="w-full py-14 lg:py-24 bg-second-gray">
                <div class="w-full xl:w-84 mx-auto px-4 md:px-10 xl:px-0">
                    <h1 class="mb-10 uppercase text-center md:text-left text-xl md:text-2xl lg:text-4xl font-medium text-white tracking-wide">about us</h1>

                    <div class="flex justify-start flex-wrap">
                        <ul class="w-60 md:w-full mx-auto md:grid md:grid-cols-3 text-white space-y-5 md:space-y-0">
                            <li class="w-full flex items-center justify-start space-x-5 mx-auto">
                                <div class="text-first-gray">
                                    <svg class="fill-current w-6" viewBox="0 0 24 24"><path stroke-width="2" d="M6.375,2 C5,2 3,3.5 2.5,4.5 C1.78539513,5.92920973 1.9033374,6.49067969 2.375,8 C3,10 4.83244154,13.545116 7.375,16 C11,19.5 14.375,21 15.875,21.5 C17.375,22 19,21.5 20,20.5 C21,19.5 22,18.5 20.875,17 C20.077805,15.9370734 18.9164827,14.7082413 17.5,14 C16.2120164,13.3560082 15.444427,13.5904184 15,14.5 C14.7543142,15.0028302 14.6780041,15.9659877 14.5,16.5 C14.2754227,17.173732 13.375,17 12.375,16.5 C11.4176235,16.0213117 9,14 7,11 C5.76086515,9.14129772 7.74150655,9.12924672 9,8.5 C10,8 10.3099909,6.84998476 9.5,5.5 C8,3 7.5,2 6.375,2 Z"></path></svg>
                                </div>
                                <div>
                                    <a href="tel:+1 323-913-4688" class="text-sm lg:text-base">+1 323-913-4688</a>
                                </div>
                            </li>

                            <li class="w-full flex items-center justify-start space-x-5 mx-auto">
                                <div class="text-first-gray">
                                    <svg class="fill-current w-6" x="0px" y="0px" viewBox="0 0 474 474" style="enable-background:new 0 0 474 474;" xml:space="preserve"><g><path d="M437.5,59.3h-401C16.4,59.3,0,75.7,0,95.8v282.4c0,20.1,16.4,36.5,36.5,36.5h401c20.1,0,36.5-16.4,36.5-36.5V95.8C474,75.7,457.6,59.3,437.5,59.3z M432.2,86.3L239.5,251.1L46.8,86.3H432.2z M447,378.2c0,5.2-4.3,9.5-9.5,9.5h-401c-5.2,0-9.5-4.3-9.5-9.5V104.9l203.7,174.2c0.1,0.1,0.3,0.2,0.4,0.3c0.1,0.1,0.3,0.2,0.4,0.3c0.3,0.2,0.5,0.4,0.8,0.5c0.1,0.1,0.2,0.1,0.3,0.2c0.4,0.2,0.8,0.4,1.2,0.6c0.1,0,0.2,0.1,0.3,0.1c0.3,0.1,0.6,0.3,1,0.4c0.1,0,0.3,0.1,0.4,0.1c0.3,0.1,0.6,0.2,0.9,0.2c0.1,0,0.3,0.1,0.4,0.1c0.3,0.1,0.7,0.1,1,0.2c0.1,0,0.2,0,0.3,0c0.4,0,0.9,0.1,1.3,0.1l0,0l0,0c0.4,0,0.9,0,1.3-0.1c0.1,0,0.2,0,0.3,0c0.3,0,0.7-0.1,1-0.2c0.1,0,0.3-0.1,0.4-0.1c0.3-0.1,0.6-0.2,0.9-0.2c0.1,0,0.3-0.1,0.4-0.1c0.3-0.1,0.6-0.2,1-0.4c0.1,0,0.2-0.1,0.3-0.1c0.4-0.2,0.8-0.4,1.2-0.6c0.1-0.1,0.2-0.1,0.3-0.2c0.3-0.2,0.5-0.3,0.8-0.5c0.1-0.1,0.3-0.2,0.4-0.3c0.1-0.1,0.3-0.2,0.4-0.3L447,109.2V378.2z"/></g></svg>
                                </div>
                                <div>
                                    <h4 class="text-sm lg:text-base">info@rentalcar.com</h4>
                                </div>
                            </li>

                            <li class="w-full flex items-center justify-start space-x-5 mx-auto">
                                <div class="text-first-gray">
                                    <svg class="fill-current w-6" x="0px" y="0px" viewBox="0 0 92 92" enable-background="new 0 0 92 92" xml:space="preserve"><path id="XMLID_507_" d="M49.9,88c-0.2,0-0.4,0-0.6-0.1c-1.8-0.3-3.2-1.7-3.3-3.5l-3.5-34.8L7.6,46.1c-1.8-0.2-3.3-1.6-3.5-3.3c-0.3-1.8,0.7-3.5,2.3-4.3l76-34.1c1.5-0.7,3.3-0.4,4.5,0.8c1.2,1.2,1.5,3,0.8,4.5l-34.1,76C52.9,87.1,51.4,88,49.9,88z M23.3,39.7L46.4,42c1.9,0.2,3.4,1.7,3.6,3.6l2.4,23.1L76,16L23.3,39.7z"/></svg>
                                </div>
                                <div>
                                    <p class="text-sm lg:text-base capitalize">4730 crystal spryngs dr, los angeles, CA 90027</p>
                                </div>
                            </li>
                        </ul>

                        <div class="w-60 md:w-[25%] mx-auto md:ml-0 mt-10 text-white tracking-wider">
                            <h2 class="uppercase text-lg font-medium text-center md:text-left">popular news</h2>
                            <div class="mt-5">
                                <h4 class="capitalize text-sm font-medium">most common dashboard warning lights</h4>
                                <h6 class="mt-3 uppercase text-xs font-medium text-first-gray">may 4, 2021</h6>
                            </div>

                            <div class="mt-6">
                                <h4 class="capitalize text-sm font-medium">car rental bookings - mobile vs desktop</h4>
                                <h6 class="mt-3 uppercase text-xs font-medium text-first-gray">may 4, 2021</h6>
                            </div>
                        </div>

                        <div class="mt-10 md:w-[60%] text-white tracking-wider">
                            <h2 class="uppercase text-lg font-medium text-center md:text-left">quick links</h2>
                            <ul class="mt-5 flex flex-wrap justify-center items-center md:justify-start spacdde-x-7 leading-8">
                                <li class="relative">
                                    <a href="#" class="ml-5 mr-7 capitalize text-sm">about us</a>
                                </li>

                                <li class="relative">
                                    <div class="w-2 h-2 rounded-full bg-blue-500"></div>
                                </li>

                                <li class="relative">
                                    <a href="#" class="ml-5 mr-7 capitalize text-sm">our cars</a>
                                </li>

                                <li class="relative">
                                    <div class="w-2 h-2 rounded-full bg-blue-500"></div>
                                </li>

                                <li class="relative">
                                    <a href="#" class="ml-5 mr-7 capitalize text-sm">blog</a>
                                </li>

                                <li class="relative">
                                    <div class="hidden lg:flex w-2 h-2 rounded-full bg-blue-500"></div>
                                </li>

                                <li class="relative">
                                    <a href="#" class="ml-5 mr-7 capitalize text-sm">services</a>
                                </li>

                                <li class="relative">
                                    <div class="w-2 h-2 rounded-full bg-blue-500"></div>
                                </li>

                                <li class="relative">
                                    <a href="#" class="ml-5 mr-7 capitalize text-sm">get in touch</a>
                                </li>
                            </ul>

                            <div class="w-full flex justify-center md:justify-start">
                                <button class="mt-8 uppercase text-sm text-white font-medium bg-blue-500 py-2 px-6">find a car</button>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section class="w-full flex flex-col md:flex-row md:justify-between md:items-center px-8 py-12 bg-footer-gray text-third-gray">
                <div class="w-full md:w-auto flex-shrink flex justify-center items-center space-x-4 text-third-gray">
                    <a href="#">
                        <svg class="fill-current w-5" viewBox="0 0 24 24"><path fill-rule="evenodd" d="M15.1742424,5.3203125 L17,5.3203125 L17,2.140625 C16.6856061,2.09765625 15.6022727,2 14.3409091,2 C11.7083333,2 9.90530303,3.65625 9.90530303,6.69921875 L9.90530303,9.5 L7,9.5 L7,13.0546875 L9.90530303,13.0546875 L9.90530303,22 L13.4659091,22 L13.4659091,13.0546875 L16.2537879,13.0546875 L16.6969697,9.5 L13.4659091,9.5 L13.4659091,7.05078125 C13.4659091,6.0234375 13.7424242,5.3203125 15.1742424,5.3203125 Z"/></svg>
                    </a>
                    <a href="#">
                        <svg class="fill-current w-4" viewBox="0 0 512 512"><path d="M492,109.5c-17.4,7.7-36,12.9-55.6,15.3c20-12,35.4-31,42.6-53.6c-18.7,11.1-39.4,19.2-61.5,23.5 C399.8,75.8,374.6,64,346.8,64c-53.5,0-96.8,43.4-96.8,96.9c0,7.6,0.8,15,2.5,22.1C172,179,100.6,140.4,52.9,81.7 c-8.3,14.3-13.1,31-13.1,48.7c0,33.6,17.1,63.3,43.1,80.7C67,210.7,52,206.3,39,199c0,0.4,0,0.8,0,1.2c0,47,33.4,86.1,77.7,95 c-8.1,2.2-16.7,3.4-25.5,3.4c-6.2,0-12.3-0.6-18.2-1.8c12.3,38.5,48.1,66.5,90.5,67.3c-33.1,26-74.9,41.5-120.3,41.5 c-7.8,0-15.5-0.5-23.1-1.4C62.9,432,113.8,448,168.4,448C346.6,448,444,300.3,444,172.2c0-4.2-0.1-8.4-0.3-12.5 C462.6,146,479,128.9,492,109.5z"/></svg>
                    </a>
                    <a href="#">
                        <svg class="fill-current w-6" viewBox="0 0 32 32" ><path d="M 11 7 C 6.027344 7 2 11.027344 2 16 C 2 20.972656 6.027344 25 11 25 C 15.972656 25 20 20.972656 20 16 C 20 15.382813 19.933594 14.78125 19.8125 14.199219 L 19.765625 14 L 11 14 L 11 17 L 17 17 C 16.523438 19.835938 13.972656 22 11 22 C 7.6875 22 5 19.3125 5 16 C 5 12.6875 7.6875 10 11 10 C 12.5 10 13.867188 10.554688 14.921875 11.464844 L 17.070313 9.359375 C 15.46875 7.894531 13.339844 7 11 7 Z M 25 11 L 25 14 L 22 14 L 22 16 L 25 16 L 25 19 L 27 19 L 27 16 L 30 16 L 30 14 L 27 14 L 27 11 Z"/></svg>
                    </a>
                    <a href="#">
                        <svg class="fill-current w-4" viewBox="0 0 600 600"><g transform="matrix(1.01619,0,0,1.01619,44,43.8384)"><path d="M251.921,0.159C183.503,0.159 174.924,0.449 148.054,1.675C121.24,2.899 102.927,7.157 86.902,13.385C70.336,19.823 56.287,28.437 42.282,42.442C28.277,56.447 19.663,70.496 13.225,87.062C6.997,103.086 2.739,121.399 1.515,148.213C0.289,175.083 0,183.662 0,252.08C0,320.497 0.289,329.076 1.515,355.946C2.739,382.76 6.997,401.073 13.225,417.097C19.663,433.663 28.277,447.712 42.282,461.718C56.287,475.723 70.336,484.337 86.902,490.775C102.927,497.002 121.24,501.261 148.054,502.484C174.924,503.71 183.503,504 251.921,504C320.338,504 328.917,503.71 355.787,502.484C382.601,501.261 400.914,497.002 416.938,490.775C433.504,484.337 447.553,475.723 461.559,461.718C475.564,447.712 484.178,433.663 490.616,417.097C496.843,401.073 501.102,382.76 502.325,355.946C503.551,329.076 503.841,320.497 503.841,252.08C503.841,183.662 503.551,175.083 502.325,148.213C501.102,121.399 496.843,103.086 490.616,87.062C484.178,70.496 475.564,56.447 461.559,42.442C447.553,28.437 433.504,19.823 416.938,13.385C400.914,7.157 382.601,2.899 355.787,1.675C328.917,0.449 320.338,0.159 251.921,0.159ZM251.921,45.551C319.186,45.551 327.154,45.807 353.718,47.019C378.28,48.14 391.619,52.244 400.496,55.693C412.255,60.263 420.647,65.723 429.462,74.538C438.278,83.353 443.737,91.746 448.307,103.504C451.757,112.381 455.861,125.72 456.981,150.282C458.193,176.846 458.45,184.814 458.45,252.08C458.45,319.345 458.193,327.313 456.981,353.877C455.861,378.439 451.757,391.778 448.307,400.655C443.737,412.414 438.278,420.806 429.462,429.621C420.647,438.437 412.255,443.896 400.496,448.466C391.619,451.916 378.28,456.02 353.718,457.14C327.158,458.352 319.191,458.609 251.921,458.609C184.65,458.609 176.684,458.352 150.123,457.14C125.561,456.02 112.222,451.916 103.345,448.466C91.586,443.896 83.194,438.437 74.378,429.621C65.563,420.806 60.103,412.414 55.534,400.655C52.084,391.778 47.98,378.439 46.859,353.877C45.647,327.313 45.391,319.345 45.391,252.08C45.391,184.814 45.647,176.846 46.859,150.282C47.98,125.72 52.084,112.381 55.534,103.504C60.103,91.746 65.563,83.353 74.378,74.538C83.194,65.723 91.586,60.263 103.345,55.693C112.222,52.244 125.561,48.14 150.123,47.019C176.687,45.807 184.655,45.551 251.921,45.551Z"/><path d="M251.921,336.053C205.543,336.053 167.947,298.457 167.947,252.08C167.947,205.702 205.543,168.106 251.921,168.106C298.298,168.106 335.894,205.702 335.894,252.08C335.894,298.457 298.298,336.053 251.921,336.053ZM251.921,122.715C180.474,122.715 122.556,180.633 122.556,252.08C122.556,323.526 180.474,381.444 251.921,381.444C323.367,381.444 381.285,323.526 381.285,252.08C381.285,180.633 323.367,122.715 251.921,122.715Z"/><path d="M416.627,117.604C416.627,134.3 403.092,147.834 386.396,147.834C369.701,147.834 356.166,134.3 356.166,117.604C356.166,100.908 369.701,87.374 386.396,87.374C403.092,87.374 416.627,100.908 416.627,117.604Z"/></g></svg>
                    </a>
                </div>
                <p class="mt-5 md:mt-0 px-14 flex-shrink md:px-0 text-center text-sm font-medium">&copy; 2021 Rent Car. All rights reserved</p>
                <p class="mt-5 md:mt-0 px-14 flex-shrink md:px-0 text-center text-sm font-medium">Privacy Policy</p>
            </section>
        </footer>

        <button class="z-[9999] fixed bottom-5 right-3 p-2 md:p-3 bg-blue-500 text-white text-lg font-medium rounded-full shadow-lg">
            <svg class="fill-current w-6 md:w-8" x="0px" y="0px" viewBox="0 0 31.301 31.301"><g><g><path d="M29.317,23.27c1.302-2.326,1.982-4.934,1.982-7.622C31.3,7.02,24.278,0,15.65,0C7.021,0,0.001,7.02,0.001,15.648c0,8.629,7.021,15.653,15.65,15.653c2.672,0,5.264-0.676,7.581-1.966c1.607,0.642,3.263,1.302,3.263,1.302c0.354,0.141,0.724,0.209,1.092,0.209c0.722,0,1.433-0.265,1.985-0.767c0.836-0.761,1.166-1.927,0.851-3.013L29.317,23.27zM27.585,27.893c0,0-2.797-1.112-4.646-1.856c-2.062,1.448-4.572,2.312-7.289,2.312c-7.012,0-12.696-5.685-12.696-12.699c0-7.011,5.684-12.695,12.696-12.695s12.697,5.683,12.697,12.694c0,2.668-0.826,5.142-2.23,7.186L27.585,27.893z"/><path d="M9.52,12.431h12.231c0.543,0,0.984-0.44,0.984-0.984c0-0.545-0.441-0.984-0.984-0.984H9.52c-0.544,0-0.984,0.439-0.984,0.984C8.535,11.991,8.976,12.431,9.52,12.431z"/><path d="M21.78,14.665H9.55c-0.543,0-0.984,0.44-0.984,0.984c0,0.544,0.441,0.984,0.984,0.984h12.23c0.545,0,0.984-0.439,0.984-0.984C22.765,15.105,22.325,14.665,21.78,14.665z"/><path d="M21.78,18.869H9.55c-0.543,0-0.984,0.439-0.984,0.984s0.441,0.983,0.984,0.983h12.23c0.545,0,0.984-0.438,0.984-0.983C22.767,19.309,22.325,18.869,21.78,18.869z"/></g></g></svg>
        </button>


        <script src="{{ asset('js/app.js') }}"></script>
        <script>
            const swiper = new Swiper.default('.swiper', {
                    spaceBetween: 20,
                    slidesPerView: 2,
                    loop: true,
                    breakpoints: {
                        320: {
                        slidesPerView: 1,
                        spaceBetween: 20
                        },

                        800: {
                        slidesPerView: 2,
                        spaceBetween: 30
                        }
                    }
                });

            const swiperOurHistory = new Swiper.default('.swiperOurHistory', {
                    loop: true,
                    breakpoints: {
                        640: {
                        slidesPerView: 2
                        },
                        800: {
                        slidesPerView: 4
                        },
                    }
                });
        </script>

        @livewireScripts
    </body>

</html>
