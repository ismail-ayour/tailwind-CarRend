<div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0">
    <div class="flex flex-col sm:justify-center items-center">
        <div class="w-full py-4 flex justify-center bg-white sm:rounded-t-lg">
            {{ $logo }}
        </div>

        <div class="w-full px-6 py-4 backdrop-blur-sm shadow-md overflow-hidden sm:rounded-b-lg">
            {{ $slot }}
        </div>
    </div>
</div>
