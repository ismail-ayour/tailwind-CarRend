<div class="swiper">
    <div class="swiper-wrapper pb-8">
        <x-comments.about-us.happy-client image="happy-client-1.jpg" name="catherine williams" status="Regular client" body="This is my second time booking with you. Not only are you the best on the coast but you have the best prices and the friendliest staff. I will be renting a car from your company now." />

        <x-comments.about-us.happy-client image="happy-client-2.jpg" name="catherine williams" status="Regular client" body="This is my second time booking with you. Not only are you the best on the coast but you have the best prices and the friendliest staff. I will be renting a car from your company now. the best prices and the friendliest staff. I will be renting a car from your company now." />

        <x-comments.about-us.happy-client image="happy-client-3.jpg" name="catherine williams" status="Regular client" body="This is my second time booking with you. Not only are you the best on the coast but you have the best prices and the friendliest staff. I will be renting a car from your company now." />

        <x-comments.about-us.happy-client image="happy-client-4.jpg" name="catherine williams" status="Regular client" body="This is my second time booking with you. Not only are you the best on the coast but you have the best prices and the friendliest staff. I will be renting a car from your company now." />
    </div>
</div>
