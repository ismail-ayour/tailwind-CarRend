@props(['image', 'name', 'status', 'body'])

<div class="swiper-slide px-4 py-7 lg:px-10 lg:py-10 bg-white shadow-md before:absolute before:bottom-0 before:left-20 before:translate-y-1/2 before:w-14 before:h-14 before:border-transparent before:border-[30px]  before:border-l-white">
    <div class="flex justify-start items-center space-x-4">
        <img src="{{ asset('imgs/' . $image) }}" class="w-20 h-20 rounded-full" alt="">
        <div>
            <h1 class="capitalize text-gray-800 font-medium text-xl">{{ $name }}</h1>
            <p class="text-blue-400 text-base">{{ $status }}</p>
        </div>
    </div>
    <div class="mt-5">
        <p class="text-sm text-first-gray leading-5 md:leading-6">
            {{ $body }}
        </p>
    </div>
</div>
