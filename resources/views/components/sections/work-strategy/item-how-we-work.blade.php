@props(['title', 'body', 'index'])

<div class="group relative w-full p-5 pb-7 bg-five-gray hover:shadow-lg transition hover:duration-1000">
    <div class="mb-2 text-blue-500">
        {{ $slot }}
    </div>
        <a href="#" class="capitalize text-lg text-gray-800 font-medium">{{ $title }}</a>
        <p class="pr-10 mt-2 text-third-gray text-sm leading-6">{{ $body }}</p>
    <span class="absolute w-12 h-12 top-0 right-0 py-1.5 px-2.5 text-2xl font-medium text-third-gray bg-white transition group-hover:duration-1000 group-hover:pt-0 group-hover:text-blue-500">0{{ $index }}</span>
</div>
