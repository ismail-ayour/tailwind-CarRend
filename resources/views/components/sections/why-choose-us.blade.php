@props(['title', 'body'])

<div class="msd:w-1/2 lsg:w-1/3 lg:flex-shrink-0 border border-first-gray">
    <div class="relative w-full h-full flex flex-col items-center py-7 before:absolute before:inset-2 before:border before:border-first-gray before:-z-10 before:transition-all before:duration-300 hover:before:inset-0">
        {{ $slot }}

        <h2 class="mb-5 capitalize text-2xl">{{ $title }}</h2>
        
        <p class="text-center leading-6 text-sm text-third-gray px-10">{{ $body }}</p>
    </div>
</div>
