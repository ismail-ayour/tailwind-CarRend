<div
                x-data="{q1:true, q2:false, q3:false}"
                class="mt-6 lg:mt-0 md:px-10 lg:px-0 w-full flex flex-col gap-4"
            >
                <div class="border border-forth-gray">
                    <div class="group relative w-full p-4 py-5 pr-28 lg:pr-4 border-b border-forth-gray"
                        x-on:click="q1 = !q1, q2 = false, q3 = false"
                    >
                        <h1 class="inline-block text-xl text-second-gray leading-[22px] tracking-wider cursor-pointer group-hover:text-blue-500">Do you provide any scripts?</h1>
                        <div class="absolute top-1/2 right-4 -translate-y-1/2 w-10 h-10 flex justify-center items-center text-second-gray border-[3px] border-blue-500 rounded-full">
                            <svg x-show="!q1" class="fill-current w-6" viewBox="0 0 24 24"><path d="M19,11H13V5a1,1,0,0,0-2,0v6H5a1,1,0,0,0,0,2h6v6a1,1,0,0,0,2,0V13h6a1,1,0,0,0,0-2Z"/></svg>
                            <svg x-show="q1" class="fill-current w-4" x="0px" y="0px" viewBox="0 0 83 83" style="enable-background:new 0 0 83 83;" xml:space="preserve"><g><path d="M81,36.166H2c-1.104,0-2,0.896-2,2v6.668c0,1.104,0.896,2,2,2h79c1.104,0,2-0.896,2-2v-6.668C83,37.062,82.104,36.166,81,36.166z"/></g></svg>
                        </div>
                    </div>
                    <div
                        x-bind:class=" q1 ? 'h-auto p-4' : 'h-0 p-0' "
                        x-clok
                        class="text-sm text-third-gray tracking-wide leading-7 overflow-hidden"
                    >
                        <p>Our templates do not include any additional scripts. Basic scripts can be easily added at www.zemez.io If you are not sure that the element you are interested in is active please contact our Support.</p>
                    </div>
                </div>

                <div class="border border-forth-gray">
                    <div class="group relative w-full p-4 py-5 pr-28 lg:pr-4 border-b border-forth-gray"
                        x-on:click="q2 = !q2, q1 = false, q3 = false"
                    >
                        <h1 class="inline-block text-xl text-second-gray leading-[22px] tracking-wider cursor-pointer group-hover:text-blue-500">What do I receive whene I order a template?</h1>
                        <div class="absolute top-1/2 right-4 -translate-y-1/2 w-10 h-10 flex justify-center items-center text-second-gray border-[3px] border-blue-500 rounded-full">
                            <svg x-show="!q2" class="fill-current w-6" viewBox="0 0 24 24"><path d="M19,11H13V5a1,1,0,0,0-2,0v6H5a1,1,0,0,0,0,2h6v6a1,1,0,0,0,2,0V13h6a1,1,0,0,0,0-2Z"/></svg>
                            <svg x-show="q2" class="fill-current w-4" x="0px" y="0px" viewBox="0 0 83 83" style="enable-background:new 0 0 83 83;" xml:space="preserve"><g><path d="M81,36.166H2c-1.104,0-2,0.896-2,2v6.668c0,1.104,0.896,2,2,2h79c1.104,0,2-0.896,2-2v-6.668C83,37.062,82.104,36.166,81,36.166z"/></g></svg>
                        </div>
                    </div>
                    <div
                        x-bind:class=" q2 ? 'h-auto p-4' : 'h-0 p-0' "
                        x-clok
                        class="text-sm text-third-gray tracking-wide leading-7 overflow-hidden"
                    >
                        <p>After you complete the payment via our secure form you will receive the instructions for downloading the product. The source files in the download package can vary based on the type of the product you have purchased.</p>
                    </div>
                </div>

                <div class="border border-forth-gray">
                    <div class="group relative w-full p-4 py-5 pr-28 lg:pr-4 border-b border-forth-gray"
                        x-on:click="q3 = !q3, q1 = false, q2 = false"
                    >
                        <h1 class="inline-block text-xl text-second-gray leading-[22px] tracking-wider cursor-pointer group-hover:text-blue-500">What am I allowed to do with a template?</h1>
                        <div class="absolute top-1/2 right-4 -translate-y-1/2 w-10 h-10 flex justify-center items-center text-second-gray border-[3px] border-blue-500 rounded-full">
                            <svg x-show="!q3" class="fill-current w-6" viewBox="0 0 24 24"><path d="M19,11H13V5a1,1,0,0,0-2,0v6H5a1,1,0,0,0,0,2h6v6a1,1,0,0,0,2,0V13h6a1,1,0,0,0,0-2Z"/></svg>
                            <svg x-show="q3" class="fill-current w-4" x="0px" y="0px" viewBox="0 0 83 83" style="enable-background:new 0 0 83 83;" xml:space="preserve"><g><path d="M81,36.166H2c-1.104,0-2,0.896-2,2v6.668c0,1.104,0.896,2,2,2h79c1.104,0,2-0.896,2-2v-6.668C83,37.062,82.104,36.166,81,36.166z"/></g></svg>
                        </div>
                    </div>
                    <div
                        x-bind:class=" q3 ? 'h-auto p-4' : 'h-0 p-0' "
                        x-clok
                        class="text-sm text-third-gray tracking-wide leading-7 overflow-hidden"
                    >
                        <p>You may build a website using the template in any way you like. You may not resell or redistribute templates (like we do); claim intellectual or exclusive ownership to any of our products, modified or unmodified. All products are property of content providing companies and individuals. You are also not allowed to make more than one project using the same template (you have to purchase the same template once more in order to make another project with the same design).</p>
                    </div>
                </div>
            </div>
