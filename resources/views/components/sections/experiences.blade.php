<section class="w-full py-16 md:py-20 bg-second-gray">
    <div class="w-full lg:w-84 mx-auto px-4 lg:px-0">
        <div class="flex items-center flex-wrap lg:flex-nowrap lg:items-start">
            <div class="w-full md:w-1/2 lg:w-full md:px-3">
                <img src="{{ asset('imgs/count-cars.jpg') }}" class="w-full object-cover">
            </div>

            <div class="w-full md:w-1/2 lg:w-full mt-12 px-5 lg:pr-32 space-y-8 md:mt-0 text-white">
                <h1 class="pl-5 py-0.5 text-[40px] font-medium tracking-wide border-l-4 border-blue-600">15+ Years of Experience</h1>
                <p class="text-[15px] font-medium text-first-gray leading-6">With more than 15 years of experience, our team offers quality car rental service to all US guests and residents.</p>
                <button class="uppercase text-sm px-7 py-3 bg-blue-500 hover:bg-third-gray">find a car</button>
            </div>

            <ul class="w-full md:mt-16 lg:mt-0 text-white uppercase px-8 lg:px-0 grid grid-cols-1 md:grid-cols-2 items-center md:justify-center text-center">
                <li class="py-6 w-full md:w-2/3 lg:w-full md:justify-self-end border-b border-first-gray">
                    <h1 class="text-6xl">20<span class="text-4xl">k</span></h1>
                    <h3 class="mt-2 text-sm">happy clients</h3>
                </li>
                <li class="py-6 w-full md:w-2/3 lg:w-full md:justify-self-start border-l border-first-gray">
                    <h1 class="text-6xl">14<span class="text-4xl">k</span></h1>
                    <h3 class="mt-2 text-sm">car</h3>
                </li>
                <li class="py-6 w-full md:w-2/3 lg:w-full md:justify-self-end md: border-r border-first-gray">
                    <h1 class="text-6xl">10</h1>
                    <h3 class="mt-2 text-sm">car type</h3>
                </li>
                <li class="py-6 w-full md:w-2/3 lg:w-full md:justify-self-start border-t border-first-gray">
                    <h1 class="text-6xl">16</h1>
                    <h3 class="mt-2 text-sm">brands</h3>
                </li>
            </ul>
        </div>


        <div class="mx-auto mt-10 flex flex-col md:flex-row flex-wrap items-center justify-center gap-8 space-y-7 md:space-y-0">
            <div class="flex justify-center items-center border border-third-gray">
                <img src="{{ asset('imgs/logo-mercedes.png') }}" alt="">
            </div>
            <div class="flex justify-center items-center border border-third-gray">
                <img src="{{ asset('imgs/logo-audi.png') }}" alt="">
            </div>
            <div class="flex justify-center items-center border border-third-gray">
                <img src="{{ asset('imgs/logo-tesla.png') }}" alt="">
            </div>
            <div class="flex justify-center items-center border border-third-gray">
                <img src="{{ asset('imgs/logo-citroen.png') }}" alt="">
            </div>
        </div>
    </div>
</section>
