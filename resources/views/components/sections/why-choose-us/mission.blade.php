<div
    x-show="mission"
    class="mt-4 md:mt-8"
>
    <div>
        <p class="text-sm tracking-wider leading-7 lg:leading-6 text-[#0fab4e] md:text-second-gray">Lorem ipsum dolor sit amet, consectetur adipisicing elit. At quae asperiores velit iure et excepturi qui perspiciatis, doloribus pariatur ipsa dignissimos officia quidem quam voluptatum.</p>
        <div class="mt-6 w-full space-y-4">
            <div class="text-gray-400 tracking-wide">
                <h1 class="text-lg uppercase">leadership</h1>
                <div class="mt-2.5 relative rounded-xl h-1.5 w-full bg-gray-200">
                    <div class="absolute left-0 h-1.5 w-[79%] bg-[#e95454]">
                        <span class="absolute -top-5 -translate-y-1/2 translate-x-1/3 right-0 text-lg text-gray-400 tracking-wide">79%</span>
                        <span class="absolute top-1/2 -translate-y-1/2 z-10 box-content right-0 w-1.5 h-1.5 bg-[#e95454] border-8 border-gray-300 shadow-xl rounded-full"></span>
                    </div>
                </div>
            </div>

            <div class="text-gray-400 tracking-wide">
                <h1 class="text-lg uppercase">customer service</h1>
                <div class="mt-2.5 relative rounded-xl h-1.5 w-full bg-gray-200">
                    <div class="absolute left-0 h-1.5 w-[70%] bg-[#e7a855]">
                        <span class="absolute -top-5 -translate-y-1/2 translate-x-1/3 right-0 text-lg text-gray-400 tracking-wide">72%</span>
                        <span class="absolute top-1/2 -translate-y-1/2 z-10 box-content right-0 w-1.5 h-1.5 bg-[#e7a855] border-8 border-gray-300 shadow-xl rounded-full"></span>
                    </div>
                </div>
            </div>

            <div class="text-gray-400 tracking-wide">
                <h1 class="text-lg uppercase">communication</h1>
                <div class="mt-2.5 relative rounded-xl h-1.5 w-full bg-gray-200">
                    <div class="absolute left-0 h-1.5 w-[88%] bg-[#247bdd]">
                        <span class="absolute -top-5 -translate-y-1/2 translate-x-1/3 right-0 text-lg text-gray-400 tracking-wide">88%</span>
                        <span class="absolute top-1/2 -translate-y-1/2 z-10 box-content right-0 w-1.5 h-1.5 bg-[#247bdd] border-8 border-gray-300 shadow-xl rounded-full"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
