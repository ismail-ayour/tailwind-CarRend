@props(['image', 'name', 'link', 'year'])

<div class="swiper-slide w-full">
    <div class="group w-full px-4">
        <div class="relative w-full overflow-hidden">
            <img src="{{asset('imgs/' . $image)}}" class="w-full h-full object-cover scale-110 transition duration-300 group-hover:scale-100" alt="">
            <div class="hidden absolute z-10 inset-0 bg-gray-800/40  group-hover:flex">
                <div class="absolute delay-300 inset-4 flex justify-center items-center border border-forth-gray text-forth-gray transition duration-500 group-hover:inset-2">
                    <svg class="fill-current w-12 translate-y-full group-hover:translate-y-0 transition duration-1000" viewBox="-2.5 -2.5 24 24"><path d='M8 14A6 6 0 1 0 8 2a6 6 0 0 0 0 12zm6.32-1.094l3.58 3.58a1 1 0 1 1-1.415 1.413l-3.58-3.58a8 8 0 1 1 1.414-1.414zM9 7h2a1 1 0 0 1 0 2H9v2a1 1 0 0 1-2 0V9H5a1 1 0 1 1 0-2h2V5a1 1 0 1 1 2 0v2z'/></svg>
                </div>
            </div>
        </div>
    </div>

    <div class="w-full mt-6">
        <h1 class="text-center px-6 text-xl font-normal text-second-gray hover:text-primary">
            <a href="{{ $link }}">{{ $name }}</a>
        </h1>

        <div class="mt-6 relative w-full h-[1px] bg-third-gray after:absolute after:left-1/2 after:-translate-x-1/2 after:top-1/2 after:-translate-y-1/2 after:z-10 after:w-3 after:h-3 after:bg-primary after:rounded-full"></div>

        <h1 class="mt-6 text-center text-xl text-primary md:tracking-wider">
            <button href="#">{{ $year }}</button>
        </h1>
    </div>
</div>
