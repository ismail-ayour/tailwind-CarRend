@props(['link', 'title'])

<a href="{{ $link }}" {{ $attributes->merge(['class' => 'flex justify-center items-center uppercase w-36 h-12 lg:h-14 text-sm font-medium']) }}>read more</a>
