@props(['categories'])

<div class="w-full md:px-5 lg:px-0 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-y-8">
    @foreach ($categories as $category)
    {{-- {{ dd($category->cars) }} --}}
        <x-cars.first-class.first-class :car="$category->cars[0]"/>
    @endforeach
</div>
