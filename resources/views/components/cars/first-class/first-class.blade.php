@props(['car'])

<div class="px-5 space-y-4 md:justify-self-center flex-shrink">
    <a href="{{ route('car-info', $car->slug) }}" class="w-full">
        <div class="h-72 w-full">
            <img src="{{ $car->image }}" class="object-fill w-full h-full transition duration-300 hover:scale-105" alt="{{ $car->name }}">
        </div>
    </a>

    <div class="md:flex items-center justify-evenly md:flex-col md:items-start space-y-4 md:space-y-4">
        <h1 class="capitalize inline-block text-2xl font-medium tracking-wide text-gray-900 hover:text-blue-400 transition duration-300">{{ $car->name }}</h1>

        <span class="block text-sm text-gray-400 tracking-wide">From ${{ $car->price }} per day</span>
    </div>
</div>
