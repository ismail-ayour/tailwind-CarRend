@props(['type'])

<div {{ $attributes->merge(['class' => 'flex text-third-gray space-x-3' ]) }}>
    {{ $slot }}
    <h4 class="uppercase text-sm font-medium text-black tracking-wide">{{ $type }}</h4>
</div>
