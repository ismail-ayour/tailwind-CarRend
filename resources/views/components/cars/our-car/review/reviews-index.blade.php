<div class="divide-y-2 divide-first-gray">
    <x-cars.our-car.review.review-item message="Thank you for the car you provided us! It is powerful and affordable." username="jessica brown on" car-name="mini cooper" />

    <x-cars.our-car.review.review-item message="The leather seats in this car look and feel amazing. Thank you!" username="ADAM WILLIAMS ON" car-name="porche panamera" />
</div>

