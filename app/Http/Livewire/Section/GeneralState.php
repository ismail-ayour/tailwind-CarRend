<?php

namespace App\Http\Livewire\Section;

use Livewire\Component;

class GeneralState extends Component
{
    public function render()
    {
        return view('livewire.section.general-state');
    }
}
