<?php

namespace App\Http\Livewire\Car;

use App\Models\Header as ModelsHeader;
use Livewire\Component;

class Header extends Component
{
    public function getHeaderProperty()
    {
        return ModelsHeader::where('page_name', 'welcome')->first();
    }

    public function render()
    {
        return view('livewire.car.header');
    }
}
