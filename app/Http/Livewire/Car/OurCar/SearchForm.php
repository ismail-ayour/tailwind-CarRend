<?php

namespace App\Http\Livewire\Car\OurCar;

use Livewire\Component;

class SearchForm extends Component
{
    public function render()
    {
        return view('livewire.car.our-car.search-form');
    }
}
