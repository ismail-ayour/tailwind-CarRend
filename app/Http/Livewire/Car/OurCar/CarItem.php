<?php

namespace App\Http\Livewire\Car\OurCar;

use Livewire\Component;

class CarItem extends Component
{
    public function render()
    {
        return view('livewire.car.our-car.car-item');
    }
}
