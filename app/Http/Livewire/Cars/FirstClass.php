<?php

namespace App\Http\Livewire\Cars;

use App\Models\Category;
use Livewire\Component;

class FirstClass extends Component
{

    public function getCategoriesProperty()
    {
        return Category::
                with('cars.images')
                ->with('cars')
                // ->with(['cars' => function($q) {
                //     $q->first();
                // }])
                ->whereHas('cars')
                ->take(6)
                ->get();
    }

    public function render()
    {
        // dd($this->categories);
        return view('livewire.cars.first-class');
    }
}
