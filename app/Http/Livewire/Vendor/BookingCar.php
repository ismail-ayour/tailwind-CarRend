<?php

namespace App\Http\Livewire\Vendor;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Barryvdh\DomPDF\Facade\Pdf;

class BookingCar extends Component
{

    public function getBookingCarsProperty()
    {
        $user = User::where('id', Auth::id())->with(['cars', 'cars.category', 'cars.energy', 'cars.images'])->first();

        return $user->cars;
    }

    public function downloadContarctCar($car)
    {
        $pdf = PDF::loadView('layouts.pdf.invoice.bookingCar');
        $pdf->setPaper('A4', 'landscape');

        return $pdf->download();
    }

    public function render()
    {
        return view('livewire.vendor.booking-car')->layout('layouts.app');
    }
}
