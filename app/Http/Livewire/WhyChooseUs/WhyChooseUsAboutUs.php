<?php

namespace App\Http\Livewire\WhyChooseUs;

use Livewire\Component;

class WhyChooseUsAboutUs extends Component
{
    public function render()
    {
        return view('livewire.why-choose-us.why-choose-us-about-us');
    }
}
