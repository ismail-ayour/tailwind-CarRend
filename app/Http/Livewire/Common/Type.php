<?php

namespace App\Http\Livewire\Common;

use Livewire\Component;

class Type extends Component
{
    public function render()
    {
        return view('livewire.common.type');
    }
}
