<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Relation::enforceMorphMap([
            'category' => 'App\Models\Category',
            'energy' => 'App\Models\Energy',
            'car' => 'App\Models\Car',
            'header' => 'App\Models\Header',
            'post' => 'App\Models\Post',
            'video' => 'App\Models\Video',
        ]);
    }
}
